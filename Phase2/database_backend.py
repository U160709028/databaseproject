import pymysql

def addstudentrecord(idStudent, Sex, Age, Adress, Grades_idGrades, ReasonType_idReasonType, SchoolName_idSchoolName, TravelTime_idTravelTime, ClassName_idClassName):
	con = pymysql.connect(host='localhost',port=3306,user='root',passwd='Darknes97.',db='mydb',charset='utf8')
	cur = con.cursor()
	a="insert into student((idStudent, Sex, Age, Adress, Grades_idGrades, ReasonType_idReasonType, SchoolName_idSchoolName, TravelTime_idTravelTime, ClassName_idClassName) values(%s,%s,%s,%s,%s,%s,%s,%s,%s);"
	b=(idStudent, Sex, Age, Adress, Grades_idGrades, ReasonType_idReasonType, SchoolName_idSchoolName, TravelTime_idTravelTime, ClassName_idClassName)
	cur.execute(a,b)
	con.commit()
	con.close()

def viewdata():
	con = pymysql.connect(host='localhost',port=3306,user='root',passwd='Darknes97.',db='mydb',charset='utf8')
	cur = con.cursor()
	cur.execute("Select * from student")
	row=cur.fetchall()
	con.commit()
	con.close()
	return row

def deletedata(idStudent):
	con = pymysql.connect(host='localhost',port=3306,user='root',passwd='Darknes97.',db='mydb',charset='utf8')
	cur = con.cursor()
	a="delete from student where StdID="+idStudent+";"
	cur.execute(a)
	con.commit()
	con.close()

def searchdata(idStudent="", Sex="", Age="", Adress="", Grades_idGrades="", ReasonType_idReasonType="", SchoolName_idSchoolName="", TravelTime_idTravelTime="", ClassName_idClassName=""):
	con = pymysql.connect(host='localhost',port=3306,user='root',passwd='Darknes97.',db='mydb',charset='utf8')
	cur = con.cursor()
	a="Select * from student where idStudent=%s and Sex=%s and Age=%s and Adress=%s and Grades_idGrades=%s and ReasonType_idReasonType=%s and SchoolName_idSchoolName=%s and TravelTime_idTravelTime=%s and ClassName_idClassName=%s;"
	b=(idStudent, Sex, Age, Adress, Grades_idGrades, ReasonType_idReasonType, SchoolName_idSchoolName, TravelTime_idTravelTime, ClassName_idClassName)
	cur.execute(a,b)
	row=cur.fetchall()
	con.commit()
	con.close()
	return row

def dataupdate(idStudent, Sex, Age, Adress, Grades_idGrades, ReasonType_idReasonType, SchoolName_idSchoolName, TravelTime_idTravelTime, ClassName_idClassName):
	con = pymysql.connect(host='localhost',port=3306,user='root',passwd='Darknes97.',db='mydb',charset='utf8')
	cur = con.cursor()
	a="update student set idStudent=%s, Sex=%s, Age=%s, Adress=%s, Grades_idGrades=%s, ReasonType_idReasonType=%s, SchoolName_idSchoolName=%s, TravelTime_idTravelTime=%s, ClassName_idClassName=%s;"
	b=(idStudent, Sex, Age, Adress, Grades_idGrades, ReasonType_idReasonType, SchoolName_idSchoolName, TravelTime_idTravelTime, ClassName_idClassName)
	cur.execute(a,b)
	con.commit()
	con.close()
  
#studentdata()