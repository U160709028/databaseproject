#Frontend
from tkinter import *
from tkinter import ttk
import tkinter.messagebox
import database_backend 

#import Student database

class Student:

	def __init__(self,root):
		self.root=root 
		self.root.title("Student Alcohol Consumption Database Project")
		self.root.geometry("1080x550")
		self.root.config(bg="Light Blue")

		idStudent=IntVar()
		Sex=StringVar()
		Age=IntVar()
		Adress=StringVar()
		Grades_idGrades=IntVar()
		ReasonType_idReasonType=IntVar()
		SchoolName_idSchoolName=IntVar()
		TravelTime_idTravelTime=IntVar()
		ClassName_idClassName=IntVar()

		def iExit():
			iExit=tkinter.messagebox.askyesno("Student Alcohol Consumption Database Project","confirm if you want to exit")
			if iExit > 0:
				root.destroy()
			return 

		def cleardata():
			self.txtStdID.delete(0,END)
			self.txtFna.delete(0,END)
			self.txtSna.delete(0,END)
			self.txtDOB.delete(0,END)
			self.txtAge.delete(0,END)
			self.txtGender.delete(0,END)
			self.txtAdress.delete(0,END)
			self.txtMobile.delete(0,END)
			self.txtClass.delete(0,END)

		def adddata():
			if (len(idStudent.get())!=0):
				database_backend.addstudentrecord(idStudent.get(),Sex.get(),Age.get(),Adress.get(),Grades_idGrades.get(),ReasonType_idReasonType.get(),SchoolName_idSchoolName.get(),TravelTime_idTravelTime.get(),ClassName_idClassName.get())
				studentlist.delete(0,END)
				studentlist.insert(END,idStudent.get(),Sex.get(),Age.get(),Adress.get(),Grades_idGrades.get(),ReasonType_idReasonType.get(),SchoolName_idSchoolName.get(),TravelTime_idTravelTime.get(),ClassName_idClassName.get())
			tkinter.messagebox.showinfo("Operation", "Entry Added")

		def displaydata():
			studentlist.delete(0,END)
			for row in database_backend.viewdata():
				studentlist.insert(END,row,str(""))

		def studentrec(event):
			global sd
			searchstduent=studentlist.curselection()[0]
			sd=studentlist.get(searchstduent)
			self.txtStdID.delete(0,END)
			self.txtStdID.insert(END,sd[1])
			self.txtFna.delete(0,END)
			self.txtFna.insert(END,sd[2])
			self.txtSna.delete(0,END)
			self.txtSna.insert(END,sd[3])
			self.txtDOB.delete(0,END)
			self.txtDOB.insert(END,sd[4])
			self.txtAge.delete(0,END)
			self.txtAge.insert(END,sd[5])
			self.txtGender.delete(0,END)
			self.txtGender.insert(END,sd[6])
			self.txtAdress.delete(0,END)
			self.txtAdress.insert(END,sd[7])
			self.txtMobile.delete(0,END)
			self.txtMobile.insert(END,sd[8])
			self.txtClass.delete(0,END)
			self.txtClass.insert(END,sd[9])
			
		def deletedata():
			if (len(idStudent.get())!=0):
				database_backend.deletedata(idStudent.get())
				cleardata()
				displaydata()
			tkinter.messagebox.showinfo("Operation", "Entry deleted")

		def searchdata():
			studentlist.delete(0,END)
			for row in database_backend.searchdata(idStudent.get(),Sex.get(),Age.get(),Adress.get(),Grades_idGrades.get(),ReasonType_idReasonType.get(),SchoolName_idSchoolName.get(),TravelTime_idTravelTime.get(),ClassName_idClassName.get()):
					studentlist.insert(END,row,str(""))

		def updatedata():
			if (len(idStudent.get())!=0):
				database_backend.deletedata(idStudent.get())
				database_backend.addstudentrecord(idStudent.get(),Sex.get(),Age.get(),Adress.get(),Grades_idGrades.get(),ReasonType_idReasonType.get(),SchoolName_idSchoolName.get(),TravelTime_idTravelTime.get(),ClassName_idClassName.get())
			tkinter.messagebox.showinfo("Operation", "Entry Updated")	

		MainFrame=Frame(self.root, bg="Light Blue")
		MainFrame.grid()	

		Titleframe=Frame(MainFrame,bd=2,padx=54,pady=8,bg="Ghost White",relief=RAISED)
		Titleframe.pack(side=TOP)

		self.lblTxt=Label(Titleframe,font=("Roboto",30,"bold"),text="Student Alcohol Consumption Database Project",bg="Ghost White")
		self.lblTxt.grid()

		Buttonframe=LabelFrame(MainFrame,bd=1,width=1350,height=70,padx=18,pady=10,bg="Ghost White",relief=RIDGE)
		Buttonframe.pack(side=BOTTOM)

		DataframeLEFT=LabelFrame(MainFrame,font=("Roboto",15,"bold"),text="Student Info",bd=1,width=1000,height=600,padx=15,pady=10,bg="Ghost White",relief=RIDGE)
		DataframeLEFT.pack(side=LEFT)

		DataframeRIGHT=LabelFrame(MainFrame,font=("Roboto",15,"bold"),text="Student Details",bd=1,width=450,height=300,padx=31,pady=3,bg="Ghost White",relief=RIDGE)
		DataframeRIGHT.pack(side=RIGHT)
		#########################
		a = ttk.Combobox(DataframeLEFT,textvariable=Sex)
		a['values']=['F','M']
		a.current(0)

		b = ttk.Combobox(DataframeLEFT,textvariable=Adress)
		b['values']=['U','R']
		b.current(0)

		c = ttk.Combobox(DataframeLEFT,textvariable=ReasonType_idReasonType)
		c['values']=[1,2,3,4]
		c.current(0)

		d = ttk.Combobox(DataframeLEFT,textvariable=SchoolName_idSchoolName)
		d['values']=[1,2]
		d.current(0)

		e = ttk.Combobox(DataframeLEFT,textvariable=TravelTime_idTravelTime)
		e['values']=[1,2,3,4]
		e.current(0)

		f = ttk.Combobox(DataframeLEFT,textvariable=ClassName_idClassName)
		f['values']=[1,2]
		f.current(0)
		

		#########################
		self.lblStdID=Label(DataframeLEFT,font=("Roboto",15),text="Student ID",padx=2,pady=2,bg="Ghost White")
		self.lblStdID.grid(row=0,column=0,sticky=W)
		self.txtStdID=Entry(DataframeLEFT,font=("Roboto",15),textvariable=idStudent,width=39)
		self.txtStdID.grid(row=0,column=1)

		self.lblFna=Label(DataframeLEFT,font=("Roboto",15),text="Sex",padx=2,pady=2,bg="Ghost White")
		self.lblFna.grid(row=1,column=0,sticky=W)
		self.txtFna=Entry(DataframeLEFT,font=("Roboto",15),textvariable=Sex,width=39)
		a.grid(row=1,column=1)

		self.lblSna=Label(DataframeLEFT,font=("Roboto",15,),text="Age",padx=2,pady=2,bg="Ghost White")
		self.lblSna.grid(row=2,column=0,sticky=W)
		self.txtSna=Entry(DataframeLEFT,font=("Roboto",15),textvariable=Age,width=39)
		self.txtSna.grid(row=2,column=1)

		self.lblDOB=Label(DataframeLEFT,font=("Roboto",15),text="Adress",padx=2,pady=2,bg="Ghost White")
		self.lblDOB.grid(row=3,column=0,sticky=W)
		self.txtDOB=Entry(DataframeLEFT,font=("Roboto",15),textvariable=Adress,width=39)
		b.grid(row=3,column=1)

		self.lblGender=Label(DataframeLEFT,font=("Roboto",15),text="ID Grades",padx=2,pady=2,bg="Ghost White")
		self.lblGender.grid(row=4,column=0,sticky=W)
		self.txtGender=Entry(DataframeLEFT,font=("Roboto",15),textvariable=Grades_idGrades,width=39)
		self.txtGender.grid(row=4,column=1)

		self.lblAge=Label(DataframeLEFT,font=("Roboto",15),text="ID ReasonType",padx=2,pady=2,bg="Ghost White")
		self.lblAge.grid(row=5,column=0,sticky=W)
		self.txtAge=Entry(DataframeLEFT,font=("Roboto",15),textvariable=ReasonType_idReasonType,width=39)
		c.grid(row=5,column=1)

		self.lblAdress=Label(DataframeLEFT,font=("Roboto",15),text="ID SchoolName",padx=2,pady=2,bg="Ghost White")
		self.lblAdress.grid(row=6,column=0,sticky=W)
		self.txtAdress=Entry(DataframeLEFT,font=("Roboto",15),textvariable=SchoolName_idSchoolName,width=39)
		d.grid(row=6,column=1)

		self.lblMobile=Label(DataframeLEFT,font=("Roboto",15,),text="ID TravelTime",padx=2,pady=2,bg="Ghost White")
		self.lblMobile.grid(row=7,column=0,sticky=W)
		self.txtMobile=Entry(DataframeLEFT,font=("Roboto",15,),textvariable=TravelTime_idTravelTime,width=39)
		e.grid(row=7,column=1)

		self.lblClass=Label(DataframeLEFT,font=("Roboto",15,),text="ID ClassName",padx=2,pady=2,bg="Ghost White")
		self.lblClass.grid(row=8,column=0,sticky=W)
		self.txtClass=Entry(DataframeLEFT,font=("Roboto",15,),textvariable=ClassName_idClassName,width=39)
		f.grid(row=8,column=1)

		def studentrec2(event):
			global sd
			searchstduent=studentlist.curselection()[0]
			sd=studentlist.get(searchstduent)
			self.txtStdID.delete(0,END)
			self.txtStdID.insert(END,sd[0])
			self.txtFna.delete(0,END)
			self.txtFna.insert(END,sd[1])
			self.txtSna.delete(0,END)
			self.txtSna.insert(END,sd[2])
			self.txtDOB.delete(0,END)
			self.txtDOB.insert(END,sd[3])
			self.txtAge.delete(0,END)
			self.txtAge.insert(END,sd[4])
			self.txtGender.delete(0,END)
			self.txtGender.insert(END,sd[5])
			self.txtAdress.delete(0,END)
			self.txtAdress.insert(END,sd[6])
			self.txtMobile.delete(0,END)
			self.txtMobile.insert(END,sd[7])
			self.txtClass.delete(0,END)
			self.txtClass.insert(END,sd[8])
			

		scrollbar=Scrollbar(DataframeRIGHT)
		scrollbar.grid(row=0,column=1,sticky='ns')

		studentlist=Listbox(DataframeRIGHT,width=41,height=16,font=('Roboto',12), yscrollcommand=scrollbar.set)
		studentlist.bind('<<ListboxSelect>>',studentrec2)
		studentlist.grid(row=0,column=0,padx=9)
		scrollbar.config(command= studentlist.yview)
		
		################### Buttons #########################

		self.btnadddate=Button(Buttonframe,text="Add New",font=("Roboto",15,'bold'),height=1,width=10,bd=4,command=adddata)
		self.btnadddate.grid(row=0,column=0)

		self.btndisplaydata=Button(Buttonframe,text="Display",font=("Roboto",15,'bold'),height=1,width=10,bd=4,command=displaydata)
		self.btndisplaydata.grid(row=0,column=1)

		self.btncleardata=Button(Buttonframe,text="Clear",font=("Roboto",15,'bold'),height=1,width=10,bd=4,command=cleardata)
		self.btncleardata.grid(row=0,column=2)

		self.btndeletedata=Button(Buttonframe,text="Delete",font=("Roboto",15,'bold'),height=1,width=10,bd=4,command=deletedata)
		self.btndeletedata.grid(row=0,column=3)

		self.btnsearchdata=Button(Buttonframe,text="Search",font=("Roboto",15,'bold'),height=1,width=10,bd=4,command=searchdata)
		self.btnsearchdata.grid(row=0,column=4)

		self.btnupdatedata=Button(Buttonframe,text="Update",font=("Roboto",15,'bold'),height=1,width=10,bd=4,command=updatedata)
		self.btnupdatedata.grid(row=0,column=5)

		self.btnexit=Button(Buttonframe,text="Exit",font=("Roboto",15,'bold'),height=1,width=10,bd=4,command=iExit)
		self.btnexit.grid(row=0,column=6)

if __name__=='__main__':
	root=Tk()
	application=Student(root)
	root.mainloop()
