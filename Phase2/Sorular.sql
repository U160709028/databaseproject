-- ----------------------------------------------------------------------------------------------
-- Questions
-- ----------------------------------------------------------------------------------------------
-- 1-Show the students who are older than 20 years and father’s job is teacher and study Portuguese class.
select student.idStudent
from student join parent on student.idStudent = parent.Student_idStudent
where age > 20 and Edutype_idEdutype = 4 and Classname_idClassName = 1;

-- 2-Show the students in math class whose reasons for choosing the school because of reputation and the student’s home address types is urban
select student.idStudent
from student
where reasontype_idReasonType = 4 and Adress = "u" and classname_idClassName = 2;

-- 3-Show the female students in the math class according to family size less or equal than three persons.
select student.idStudent
from student join parent on student.idStudent = parent.Student_idStudent
where  sex = "f" and classname_idclassname = 1 and Famsize = "LE3";

-- 4-Show the male students in math class whose current health status is greater than 4
select student.idStudent
from student join alcoholcons on student.idStudent = alcoholcons.Student_idStudent
where health > 4 and sex = "M" and classname_idClassName = 1;

-- 5-Show the students workday alcohol consumption is less than 3 and weekend alcohol consumption data is equal and smaller than 2.
select alcoholcons.Student_idStudent
from alcoholcons
where  Dalc < 3 and Walc <= 2;

-- 6-Show the students who have free time above 3 and who can have time to go out after the school below 2 in math class.
select student.idStudent
from student join statvalues on student.idStudent = statvalues.Student_idStudent
			 join alcoholcons on student.idStudent = alcoholcons.Student_idStudent
where statvalues.StatType_idStatType = 3 and statvalues.Values > 3 and alcoholcons.Goout < 2 and classname_idClassName = 2;

-- 7-Show the students who get school support and family support in portuguese language course.
select student.idStudent
from student join statvalues on student.idStudent = statvalues.Student_idStudent
			 join parent on student.idStudent = parent.Student_idStudent
where statvalues.StatType_idStatType = 10 and statvalues.Values = 1 and parent.Famsub = 1 and classname_idClassName = 1;

-- 8-Show the students’s Average of midterm 2 in Gabriel Pereira’s portuguese language course
select avg(Grade) 
from student join grades on student.idStudent = grades.idGrades
where classname_idClassName = 1 and schoolname_idSchoolName = 1 and grades.Preiod = 2;

-- 9-Show the female students whose get the highest final grade on Mathematic class in Mousinho da Silveira.
select student.idStudent, grades.Grade
from student join grades on student.idStudent = grades.idGrades
where sex = "F" and classname_idClassName = 2 and schoolname_idSchoolName = 2 and grades.preiod = 3
order by grade desc
Limit 1;

-- 10-Show the female students have free time after school is greater than 3 and final grade is greater than 10 in math class.
select student.idStudent
from student join statvalues on student.idStudent = statvalues.Student_idStudent
			 join grades on student.idStudent = grades.idGrades
where sex = "f" and statvalues.StatType_idStatType = 3 and statvalues.Values > 3 and classname_idClassName = 2 and grades.Preiod = 3 and grades.Grade > 10;

-- ----------------------------------------------------
-- View,Insert,Update,Delete,Group
-- ----------------------------------------------------
-- View 1
create view Alcoholic as
select alcoholcons.Student_idStudent
from alcoholcons
where Dalc > 4 and Walc > 4;
-- Try
select *
from alcoholic join student on student.idStudent = alcoholic.Student_idStudent
where Sex = "F";
-- View 2 --Absences
create view absencesV as
select student.idStudent, statvalues.Values
from student join statvalues on student.idStudent = statvalues.Student_idStudent
where statvalues.StatType_idStatType = 1;
-- Try
select *
from absencesV join student on student.idStudent = absencesv.idStudent
where absencesv.values > 10;

-- Group
select student.age, count(student.idStudent)
from student
group by age;

-- Insert
insert into grades (grades.idGrades,grades.Grade,grades.Preiod)
values (1045,15,1);
insert into grades (grades.idGrades,grades.Grade,grades.Preiod)
values (1045,15,2);
insert into grades (grades.idGrades,grades.Grade,grades.Preiod)
values (1045,15,3);

Insert Into student (idStudent,Sex,Age,Adress,Grades_idGrades,ReasonType_idReasonType,SchoolName_idSchoolName,TravelTime_idTravelTime,ClassName_idClassName)
Values (1045, "M", 21 , "U" , 1045,3,1,1,1);

-- Update
Update student
set student.idStudent = 1046
where student.idStudent = 1045;

-- Delete
Delete 
from student 
where student.idStudent = 1046;

-- Index
create index id_indx on student (Sex);
alter table student drop index id_indx;

-- Stored Procedures
DELIMITER //
CREATE PROCEDURE AvgGrades(in idgra int, out G2 int)
BEGIN
	SELECT Avg(grades.grade)
    into G2
	FROM grades
	WHERE grades.idGrades = idgra;
END //
DELIMITER ;
Drop procedure AvgGrades;
Call AvgGrades(1044, @G22);
select @G22;

-- Stored Procedures 2
DELIMITER //
CREATE PROCEDURE Edu2(in G0 int, in G1 varchar(30))
BEGIN
	insert into edutype (edutype.idEdutype,edutype.Type)
    values (G0,G1);
END //
DELIMITER ;
Drop procedure Edu2;
Call Edu2(7, "Sander");
Delete From edutype where edutype.idEdutype = 7;